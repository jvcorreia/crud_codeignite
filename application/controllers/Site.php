<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('site/index');
	}


	public function listar_livros()
	{
		$this->load->model('Livro');// caregando o modelo
		$data['livros'] = $this->Livro->listarLivros();
		$this->load->view('site/listar_livros',$data);
	}

	public function listar_livro($id=NULL){
		$id = $_POST['idlivro'];
		if($id != NULL){

		$this->load->model('Livro');// caregando o modelo

		$data['livro'] = $this->Livro->getLivroById($id);

		$this->load->view('site/listar_livro',$data);
		}
		else{
			$this->load->view('site/listar_livro');
		}

	}

	public function listar_tudo($id=NULL){
		if($id != NULL){

			$this->load->model('Livro');// caregando o modelo
	
			$data['livro'] = $this->Livro->listarLivrosEAutores($id);
	
			$this->load->view('site/listar_livro',$data);
			}
			else{
				$this->load->view('site/listar_livro');
			}
	}


}
