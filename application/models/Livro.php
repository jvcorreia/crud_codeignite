<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Livro extends CI_Model{
    public function listarLivros(){
        //$this->db->order_by('nome', 'acs');//Usando order by
        //$this->db->where('id_livro',3);//Fazendo uma query especifica
        $query = $this->db->get('livro');//Função para consultar dados
        return $query->result();
    }

    public function getLivroById($id = NULL){

            $this->db->where('id_livro',$id);//Fazendo uma query especifica
            $query = $this->db->get('livro');//Função para consultar dados
            return $query->row();
       
    }

    public function listarLivrosEAutores($id = NULL){

        $this->db->select('livro.*, autor.nome');
        $this->db->from('livro');
        $this->db->join('autor','livro.id_livro = autor.id_livro');
        $this->db->where('livro.id_livro',$id);//Fazendo uma query especifica
        $query = $this->db->get();
        return $query->row();

    }
}


?>